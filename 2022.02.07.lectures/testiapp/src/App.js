import { useState } from 'react';
import logo from './logo.svg';
import './App.css';

const ToinenKomponentti = (props) => {

  const [nimi, setNimi] = useState('Hepokatti');
  const handleNameChange = () => {
    nimi === 'Hepokatti' ? setNimi('Muurahainen') : setNimi('Hepokatti');
  }

  return (
    <>
      <h1 onClick={props.funktio} >{props.teksti}</h1>
      <h1 onClick={handleNameChange}> {props.toinenTeksti} </h1>
      <h2> Nimi on {nimi} </h2>
  
    </>
  )

}

const App = () => {

  const logger = () => {
    console.log("Nonni!");
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Updated 2022.02.07 at 10:36. Edit <code>src/App.js</code> and save to reload.
        </p>

          Learn React
          <ToinenKomponentti 
            teksti="MoroMoro"
            toinenTeksti ="HeipäHei!"
            funktio={logger}
            />
      </header>
    </div>
  );
}

export default App;
