import { useEffect, useState } from 'react';
import './App.css';

const Clock = () => {
  const [time,setTime] = useState(new Date());

  useEffect( () => {
    const counter = setInterval( () => {
      setTime(new Date());      
    }, 1000);
    return () => {
      clearInterval(counter);
    }, [];
  });


  return (
    <div className='clock-container'>
      <div className='time'>
        {time.toLocaleTimeString("en-GB")}
      </div>
      <div className='date'>
      {time.toLocaleDateString("en-GB")}
      </div>
    </div>
  );
}

export default Clock;
