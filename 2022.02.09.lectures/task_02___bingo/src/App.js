import './App.css';
import React, {useState} from "react";
import { v4 as uuidv4 } from "uuid";

function App() {

  const [numbers, setNumbers] = useState([]);
  const numbersDOM = numbers.map(  (number) => {
    const key = uuidv4();
        return (
            <li key={key} className="circle"> {number} 
            </li>  
        );
  });

  const updateNumbers = () => {
    let newNum;
    // make sure to get a new number
    while (numbers.includes(newNum = Math.floor(Math.random()*40))) {}
    setNumbers([...numbers, newNum]);
  }

  const resetNumbers = () => {
    setNumbers([]);
  }

  return (
    <div className="App">
      <header className="App-header">
      <div className='numbers'>
         <ul className="no-bullets">{numbersDOM}</ul>
      </div>
      <button className="button-77" type="button" onClick={updateNumbers}>+</button> 
      <button className="button-77" type="button" onClick={resetNumbers}>Reset</button> 
      </header>
    </div>
  );
}

export default App;
