import axios from "axios";

const baseURL = "https://data.itsfactory.fi/journeys/api/1/lines";

export const getAllBusLines = async () => {

    const result = await axios.get(`${baseURL}`);
    // console.log(result.data.body);
    return result.data.body;
};