import React, { useState, useEffect} from "react";
import "./App.css";
import { getAllBusLines } from "./services/buslineService.js";

const  App = () =>  {

    const [busLines, setBuslines] = useState([]);
    useEffect(  () => {
        const getBusLines = async () => {
            const data = await getAllBusLines();
            setBuslines(data);
        };
        getBusLines();
    // useEffect : to avoid infinite loop, must have
    }, []);

    const buslinesDOM = busLines.map(  (busLine) => {
        return (
            <tr key={busLine.name}>
                <td> {busLine.name} </td>
                <td> {busLine.description} </td>
            </tr>
        );
    });

    return (
        <div className="App">
            <header className="App-header">
                <table className="myTable">
                    {buslinesDOM}
                </table>                
            </header>
        </div>
    );

};

export default App;
