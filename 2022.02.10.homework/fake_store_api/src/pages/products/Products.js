// https://reactjs.org/docs/handling-events.html

import React, { useState, useEffect, useRef} from "react";
import NewWindow from "react-new-window";
import Select from "react-dropdown-select";
import { getAllProducts, getAllCategories } from "../../services/getProducts.js";
import "./Products.css";

const Products = () => {

    const [products, setProducts] = useState([]);
    const [productsJSX, setProductsJSX] = useState([]);

    const [categories, setCategories] = useState([]);
    // state to keep track of loading data
    const [loadingData, setLoadingData] = useState(false);
    // whether or not to display just a single product
    const [singleProductDisplay, setSingleProductDisplay] = useState(false);
    const [singleProduct, setSingleProduct] = useState([]);
    // State related to modifying a product
    const [modifyProductState, setModifyProductState] = useState(false);
    let modifyProductJSX = "";
    const modifyFormRef = useRef(null);

    useEffect(  () => {
        const updateInformation = async () => {

            try {
                // set data to loading
                setLoadingData(true);
                await Promise.all([
                    setProducts(await getAllProducts()),
                    setCategories(await getAllCategories())
                ]);
                // set loading to false (done)
                setLoadingData(false);
                updateProductsJSX("all");
            } catch (error) {
                console.log("Error in retrieving information");
            }
        };

        updateInformation();
    // useEffect : to avoid infinite loop, must have
    }, []);

    // update products JSX if switching back from single product display
    useEffect( () => {
        updateProductsJSX("all");
    }, [singleProductDisplay]);

    const updateProductsJSX = (targetCategory) => {
  
        /*
        console.log("Selected category is :");
        console.log(targetCategory);
        console.log("Products are:");
        console.log(products);
        */

        let filteredProducts = products;
        if ( (targetCategory !== "all") ) {
            filteredProducts = products.filter( (product) => {
                return (product.category === targetCategory);
            });
        }

        /*
        console.log("Filtered products are:");
        console.log(filteredProducts);
        */

        const JSX = filteredProducts.map( (product) => {
            return (
                <div key={product.id} onClick={(e) => {
                    setSingleProductDisplay(true);
                    setSingleProduct(product);
                }}> 
                    <img src={product.image} width="80" height="80"/> <br/> 
                    {product.title} <br/>
                    {product.price} €
                </div>
            );
        });

        setProductsJSX(JSX);
    };

    if (loadingData || products.length === 0 || categories.length === 0) {
        return  (
            <div className="products-main">
                <h1> Loading data.... </h1>
                <p>  Please wait</p>
            </div>
        );
    } else if (!singleProductDisplay) {
        
        // Make sure there is something to display (asynchronous loading)
        if (productsJSX.length === 0) {
            // console.log("Updating view");
            updateProductsJSX("all");
        }

        // default option is all, then add the rest of the loaded categories
        const categoriesOptions = [];
        categoriesOptions[0] = {label: "all", value: "all"};
        for (let i=0; i<categories.length;i++) {
            categoriesOptions[i+1] = {label: categories[i], value: categories[i]};
        }

        return (
            <div className="products-main">
                <div className="shoptitle">
                    <h1>FakeStore</h1>
                </div>
                <div className="products-dropdown">
                    <Select options={categoriesOptions} dropdownPosition="auto" onChange={(values) => updateProductsJSX(values[0].value)}/>
                </div>
                <div className="products-wrapper"> 
                    {productsJSX}
                </div>
            </div>
        );
    } else {
        if (modifyProductState) {
            const windowFeatures = [{width:300, height:300}];

            modifyProductJSX = (
                <NewWindow features={windowFeatures} title="Modify product">
                    <h1>Modifying product</h1>
                    <form ref={modifyFormRef}>
                        <table className="products-table"> 
                            <tbody>
                                <tr> 
                                    <td> &nbsp; </td> 
                                    <td> <img src={singleProduct.image} width="80" height="80"/> </td>
                                </tr>
                                <tr>
                                    <td> title </td> 
                                    <td>  <input type="text" id="titleInput" value={singleProduct.title} size="50" onChange={(e) => {
                                        console.log(e.target.value);
                                        singleProduct.title = e.target.value;
                                    }}
                                    /></td>
                                </tr>
                                <tr>
                                    <td> description </td> 
                                    <td>  {singleProduct.description} </td>
                                </tr>
                                <tr>
                                    <td> category </td> 
                                    <td>  {singleProduct.category} </td>
                                </tr>
                                <tr>
                                    <td> rating </td> 
                                    <td>  {singleProduct.rating.rate} </td>
                                </tr>
                                <tr>
                                    <td> price </td> 
                                    <td>  {singleProduct.price} € </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>

                    <button onClick={ (e) => {
                        console.log(modifyFormRef);
                        setModifyProductState(false);
                    }}
                    > Submit changes
                    </button>

                </NewWindow>
            );
        }

        return (
            <div className="products-main">
                <h1> Now in Single product display. </h1>
                <button onClick={ (e) => setSingleProductDisplay(false)}> Go back to all products display. </button>
                <table className="products-table"> 
                    <tbody>
                        <tr> 
                            <td> &nbsp; </td> 
                            <td> <img src={singleProduct.image} width="80" height="80"/> </td>
                        </tr>
                        <tr>
                            <td> title </td> 
                            <td>  {singleProduct.title} </td>
                        </tr>
                        <tr>
                            <td> description </td> 
                            <td>  {singleProduct.description} </td>
                        </tr>
                        <tr>
                            <td> category </td> 
                            <td>  {singleProduct.category} </td>
                        </tr>
                        <tr>
                            <td> rating </td> 
                            <td>  {singleProduct.rating.rate} </td>
                        </tr>
                        <tr>
                            <td> price </td> 
                            <td>  {singleProduct.price} € </td>
                        </tr>
                    </tbody>
                </table>
                <button onClick={ (e) => {
                    setModifyProductState(true);
                }}
                > Modify the product
                </button>
                <button onClick={ (e) => {
                    const filteredProducts = products.filter( (product) => {
                        return (product.id !== singleProduct.id);
                    });
                    setProducts(filteredProducts);
                    setSingleProductDisplay(false); 
                }}
                > Delete the product
                </button>
                {modifyProductJSX}
            </div>

        );


    }

};

export default Products;