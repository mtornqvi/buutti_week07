// Refer to : https://www.postgresql.org/docs/13/datatype.html
// version 13


import db from "../db/db.js";

const findAll = async () => {
    console.log("Requesting for all products...");
    const result = await db.executeQuery(
        "SELECT * FROM \"products\";"
    );
    console.log(`Found ${result.rowCount} products.`);
    return result;
};

export default {
    findAll
};