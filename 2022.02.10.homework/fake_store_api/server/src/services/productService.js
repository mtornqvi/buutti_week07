import productDao from "../dao/productDao.js";

const findAll = async () => {
    const products = await productDao.findAll();
    return products.rows;
};

export default { 
    findAll
};