// 

import express from "express";
import dotenv from "dotenv"; // for passwords
import helmet from "helmet"; // for security
import "express-async-errors";  // enables middleware to handle errors correctly
import logger from "./services/logger.js"; // for logging all queries
import rootRouter from "./api/rootRouter.js";
import productRouter from "./api/productRouter.js";
import db from "./db/db.js";
import { 
    unknownEndpoint, 
    errorHandler
} from "./middlewares.js";

process.env.NODE_ENV != "prod" && dotenv.config();

const app = express();
app.use (express.json()); // this must be used to parse POST request body made in JSON format (see in Postman)

// enable all helmet security policies, refer to https://github.com/helmetjs/helmet
// details on effects : https://blog.logrocket.com/express-middleware-a-complete-guide/#helmet
// e.g. removed of X-Powered-By from headers
app.use(helmet());

// Start logging requests
app.use(logger);

// Routing
app.use(express.static("public"));
app.use("/", rootRouter);
app.use("/products", productRouter); // everything under /products
app.use(unknownEndpoint);
app.use(errorHandler);

process.env.NODE_ENV !== "test" && db.createTables();
const APP_PORT = process.env.APP_PORT || 8080;

(process.env.NODE_ENV !== "test") && app.listen(APP_PORT,  ()  => {
    console.log (`Listening to port ${APP_PORT}`);
});

// must export the Express function => otherwise timeout will occur
export default app;

