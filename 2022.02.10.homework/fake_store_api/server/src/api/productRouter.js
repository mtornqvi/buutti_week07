import { Router } from "express";
import productService from "../services/productService.js";

const router = Router();

router.get("/", async (_req, res) => {
    const products = await productService.findAll();
    res.status(200).send(products);
    return;
});

export default router;