// https://reactjs.org/docs/forms.html
// https://reactjs.org/docs/dom-elements.html
import "./App.css";
import { v4 as uuidv4 } from "uuid";
import React, {useRef, useState} from "react";

const App = () => {
    
    // useRef!
    const inputForm = useRef(null); 
    const [todoList, setTodoList] = useState([]);

    const updateTodoList = () => {
        const form = inputForm.current;
        const input = form["todoinput"].value;
        setTodoList ( (oldArray) => [...oldArray,input]);
    };

    const handleListClicks = (e,targetElement) => {
        // if pressed on the left
        // change class => refer to App.css
        if (e.clientX < 25) {
            if (e.target.className === "") {
                e.target.className = "check";
            } else {
                e.target.className = "";   
            }
        }


        // if pressed on the right
        if (e.view.window.innerWidth - e.clientX < 35) {
            const filtered = todoList.filter( (element) => {return element !== targetElement;});
            setTodoList(filtered);
        }

    };

    // must have unique key
    const listItems = todoList.map(  (element) => {
        const key = uuidv4();
        return (
            <li key={key} onClick={ (e) => handleListClicks(e,element)} className=""> {element} 
            </li>  
        );
    });

    // Display part
    return (
        <div className="App">
            <div className="App-header">
                <form ref={inputForm}>
                    <label htmlFor="todoinput">New todo item:</label><br/>
                    <input type="text" id="todoinput" name="todoinput"/>
                    <button type="button" onClick={updateTodoList}>insert</button> 
                </form> 
            </div>
            <div className="checkbox">
                <ul className="no-bullets">{listItems}</ul>
            </div>
        </div>
    );
};

export default App;
