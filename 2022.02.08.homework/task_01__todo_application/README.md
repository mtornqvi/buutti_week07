## Task 1 - Todo application

Create a React application with an input bar and a button to add a new Todo item, and a view to see all todos in the list
- Each item should be toggleable for done or not done
- Each added item to the list should also be able to be removed / discarded

- **EXTRA** Give the UI a nice look

### Things learned during project
- Handing a parameters + event handler to function:
onClick={ (e) => handleListClicks(e,element)}
- using "useRef, usestate":
import React, {useRef, useState} from "react";

    const inputForm = useRef(null); 

    const updateTodoList = () => {
        const form = inputForm.current;
        const input = form["todoinput"].value;
        setTodoList ( (oldArray) => [...oldArray,input]);
    };

    <form ref={inputForm}>


## **EXTRA** Task 2 - Memory game (difficult)

Create a memory game, i.e. you have 16 cards, or 8 pairs of cards, with some symbols or images (you choose). When the game starts, every card should be the same, and then the player can click one card to reveal it, then click another card to reveal it too. If the cards are the same, they should be removed. If the cards are not the same, they should be hidden again.
- **EXTRA:** Allow the player to choose the amount of cards to be played with between 2 pairs (4 cards) and 32 pairs (64 cards).
- **EXTRA:** Count the number of tries to complete the game.
- **EXTRA: (harder)** Add some animation when the cards open/close, meaning they don't instantly just flash open or close, but are revealed over 1 second in some animation.
